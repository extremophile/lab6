#include <iostream>
#include <math.h>
#include <string>

using namespace std;

int main()
{
cout << "Enter deposit:" << endl;
double deposit;
cin >> deposit;
cout << "Enter year percent:" << endl;
double percent;
cin >> percent;
cout << "Enter years:" << endl;
double years;
cin >> years;

double powered = pow(1 + percent/100, years);

double compoundInterest = deposit*powered;
cout << "The compound interest is: " << compoundInterest << endl;
}
