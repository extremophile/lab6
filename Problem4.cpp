#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

int main()
{
cout << "Enter faculty number, name and number of grades:" << endl;
string facultyNumber, firstName, lastName;
double numberOfGrades;
while (cin >> facultyNumber)
{
double grade;
double sum = 0;
cin >> firstName >> lastName;
cin >> numberOfGrades;
for(int i = 0; i < numberOfGrades; i++)
{
cout << "Enter grade:" << endl;
cin >> grade;
sum += grade;
}
cout << fixed << setprecision(2);

double average = sum/numberOfGrades;
cout << "Faculty number:" << setw(12) << "Grade" << endl;
cout << facultyNumber << setw(20) << average << endl;
}
return 0;
}
